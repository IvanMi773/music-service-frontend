import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import config from '../../configuration'
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
import { AuthResponse } from 'src/app/models/AuthResponse';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    
    public loginForm
	@Output() onLoggedIn = new EventEmitter<boolean>();
    private _serverErrorResponse: any;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
		private tokenStorage: TokenStorageService,
		private router: Router
    ) {}

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required, Validators.minLength(this.usernameMinLength)]],
            password: ['', [Validators.required, Validators.minLength(this.passwordMinLength)]],
        });
    }

	onSubmit() {
        this.userService
            .login(this.username.value, this.password.value)
			.subscribe(
				(data: AuthResponse) => {
                    this.tokenStorage.signOut()

					this.tokenStorage.register(this.tokenStorage.tokenKey, data.token)

					this.setOnLoggedIn()
					this.router.navigateByUrl('/home');
				},
				error => this._serverErrorResponse = error.status
			)
    }

	setOnLoggedIn(){
		this.onLoggedIn.emit(true);
	}

    get username() {
        return this.loginForm.get('username')
    }
    get password() {
        return this.loginForm.get('password')
    }
	get usernameMinLength() {
        return config.validators.usernameMinLength
    }
    get passwordMinLength() {
        return config.validators.passwordMinLength
    }
    public get serverErrorResponse(): any {
        return this._serverErrorResponse;
    }
    public set serverErrorResponse(value: any) {
        this._serverErrorResponse = value;
    }
}
