import { Playlist } from "./Playlist";

export interface PlaylistChooseDialogData {
    playlists: Array<Playlist>
}
