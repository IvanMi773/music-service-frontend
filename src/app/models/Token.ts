export interface Token {
    authorities: string
    sub: string
}
