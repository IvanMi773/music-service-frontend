import { User } from "./User";

export interface UserSubscriptions {
    subscribers: Array<User>
    subscriptions: Array<User>
}
